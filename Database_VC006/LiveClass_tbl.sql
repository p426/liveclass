USE [LiveClassDb]
GO

/****** Object:  Table [dbo].[LiveClasses]    Script Date: 10/8/2020 10:09:28 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LiveClasses](
	[LiveClassId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](255) NULL,
	[SubjectId] [int] NULL,
	[TeacherId] [int] NULL,
	[Date] [date] NULL,
	[StartTime] [time](0) NULL,
	[Duration] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[LiveClassId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[LiveClasses]  WITH CHECK ADD FOREIGN KEY([SubjectId])
REFERENCES [dbo].[Subject] ([SubjectId])
GO

ALTER TABLE [dbo].[LiveClasses]  WITH CHECK ADD FOREIGN KEY([TeacherId])
REFERENCES [dbo].[Teachers] ([TeacherId])
GO

ALTER TABLE [dbo].[LiveClasses]  WITH CHECK ADD CHECK  (([Duration]=(45) OR [Duration]=(60) OR [Duration]=(75) OR [Duration]=(90) OR [Duration]=(105) OR [Duration]=(120)))
GO


