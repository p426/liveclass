USE [LiveClassDb]
GO

/****** Object:  Table [dbo].[LiveClassUser]    Script Date: 10/8/2020 10:09:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LiveClassUser](
	[LiveClassUserId] [int] IDENTITY(1,1) NOT NULL,
	[LiveClassId] [int] NULL,
	[UserId] [int] NULL,
	[IsAttended] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[LiveClassUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[LiveClassUser]  WITH CHECK ADD FOREIGN KEY([LiveClassId])
REFERENCES [dbo].[LiveClasses] ([LiveClassId])
GO

ALTER TABLE [dbo].[LiveClassUser]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[Registration] ([UserId])
GO


