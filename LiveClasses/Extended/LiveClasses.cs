﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace LiveClasses
{
    [MetadataType(typeof(partialClass))]
    public partial class LiveClasses
    {
    }
    public class partialClass
    {
        public int LiveClassId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public int? SubjectId { get; set; }
        [Required]
        public int? TeacherId { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Date { get; set; }
        [Required]
        [DataType(DataType.Time)]
        public DateTime StartTime { get; set; }
        [Required]
        public int? Duration { get; set; }
    }
}