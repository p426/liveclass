﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LiveClasses.Models
{
    public class LiveClassData7
    {
        public class LiveClassData
        {
            public int? SubjectId { get; set; }
            public string Title { get; set; }
            public int? Duration { get; set; }
            public DateTime? Date { get; set; }
            public TimeSpan? Time { get; set; }
        }

        public class CompletedLiveClassdata
        {
            public int? SubjectId { get; set; }
            public string Title { get; set; }
            public int? Duration { get; set; }
            public DateTime? Date { get; set; }
            public TimeSpan? Time { get; set; }
        }
        public class TeachersLiveClassdata
        {
            public int? Id { get; set; }
            public int? TotalCount { get; set; }
            public string Name { get; set; }

        }
    }
}