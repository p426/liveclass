﻿using LiveClasses.Models;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using static LiveClasses.Models.DashboardData;

namespace LiveClasses.Controllers
{
    public class DashboardController : Controller
    {
        // GET: Dashboard
        public ActionResult Index()
        {
            //Total Live Classes ( Count of the Live Classes) 
            LiveClassDbEntities db = new LiveClassDbEntities();
            var TotalLiveClasses = db.LiveClasses.ToList().Count();
            ViewBag.TotalLiveClass = TotalLiveClasses;

            //Total Number of Student Attended
            var TotalUsers = db.LiveClassUser.ToList();
            var TotalNoOfStudentAttendClass = TotalUsers.Where(a => a.IsAttended == true).Count();
            ViewBag.TotalAttendence = TotalNoOfStudentAttendClass;

            //Total Teachers
            var TotalTeachers = (from p in db.LiveClasses join t in db.Teachers on p.TeacherId equals t.TeacherId select new { p.TeacherId }).ToList().Distinct().Count();
            ViewBag.TotalTeachers = TotalTeachers;

            DateTime currentDate = DateTime.Today;
            var currentTime = DateTime.Now.TimeOfDay;

            //Up Coming Live Classes
            var dates = db.LiveClasses.Where(a => a.Date >= currentDate).ToList();
            List<LiveClassData> upComingLiveClass = new List<LiveClassData>();
            foreach (var item in dates)
            {
                if (item.Date == currentDate)
                {

                    if (item.StartTime > currentTime)
                    {
                        upComingLiveClass.Add(new LiveClassData { Subject = item.Subject.Name, Title = item.Title, Duration = item.Duration, Date = item.Date, Time = item.StartTime });
                    }
                }
                else
                {
                    upComingLiveClass.Add(new LiveClassData { Subject= item.Subject.Name, Title = item.Title, Duration = item.Duration, Date = item.Date, Time = item.StartTime });
                }
            }
            ViewBag.UpComingLiveClass = upComingLiveClass;
            //Completed Live Classes 

            var completedLiveTask = db.LiveClasses.Where(a => a.Date <= currentDate).ToList();
            List<CompletedLiveClassdata> CompletedLiveClass = new List<CompletedLiveClassdata>();

            foreach (var item in completedLiveTask)
            {
                if (item.Date == currentDate)
                {
                    if (item.StartTime <= currentTime)
                    {
                        CompletedLiveClass.Add(new CompletedLiveClassdata { Subject = item.Subject.Name, Title = item.Title, Duration = item.Duration, Date = item.Date, Time = item.StartTime });
                    }
                }
                else
                {
                    CompletedLiveClass.Add(new CompletedLiveClassdata { Subject = item.Subject.Name, Title = item.Title, Duration = item.Duration, Date = item.Date, Time = item.StartTime });
                }
            }
            ViewBag.CompletedLiveClass = CompletedLiveClass;

            #region // Top 5 Teachers who take maximum Live Classes
            Dictionary<int?, int?> Groupvalues = new Dictionary<int?, int?>();

            List<TeachersLiveClassdata> TeachersData = new List<TeachersLiveClassdata>();

            var result = (from p in db.LiveClasses group p by p.TeacherId into lgroup
                      select new {
                          id = lgroup.Key,
                          count = lgroup.Count()
                      }).ToList();
            foreach (var item in result)
            {
                Groupvalues.Add(item.id,item.count);
            }
            foreach(var val in Groupvalues)
            {
                foreach (var item in db.Teachers)
                {
                    if (val.Key == item.TeacherId)
                    {
                        TeachersData.Add(new TeachersLiveClassdata {Id=item.TeacherId,Name=item.FirstName,TotalCount=val.Value });
                    }
                }
            }
            var topFiveMaximumClasses= TeachersData.OrderByDescending(x => x.TotalCount).Take(5);
            ViewBag.TopFiveMaximumClasses = topFiveMaximumClasses;

            #endregion

            #region // Top 5 Live Classes with maximum student attend

            List<MaxAttendenceClassdata> maxAttendences = new List<MaxAttendenceClassdata>();

            var resultdata = (from p in db.LiveClassUser
                              where p.IsAttended==true
                              group p by p.LiveClassId into lgroup
                          select new
                          {
                              id = lgroup.Key,
                              count = lgroup.Count()
                          }).ToList();

            foreach (var item in resultdata)
            {
                maxAttendences.Add(new MaxAttendenceClassdata { LiveId = item.id, TotalCount = item.count });

            }
            ViewBag.MaxAttendences = maxAttendences.Take(5);
            #endregion


            return View();
        }

        public ActionResult Index1()
        {
            return View();
        }
    }
}
