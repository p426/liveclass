﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace LiveClasses.Controllers
{
    public class LiveClassController : Controller
    {
        // GET: LiveClass
        private LiveClassDbEntities db = new LiveClassDbEntities();
        public ActionResult Index(int? page)
        {
            var data = db.LiveClasses.ToList().ToPagedList(page??1,5);
            //(page??1,3) here the coalescene function is used if the page value is null then it assign it to 1 and here 3 is page size
            return View(data);
        }

        // GET: LiveClass/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: LiveClass/Create
        public ActionResult Add()
        {
            ViewBag.SubjectId = new SelectList(db.Subject, "SubjectId", "Name");
            ViewBag.TeacherId = new SelectList(db.Teachers, "TeacherId", "FirstName");
            return View();
        }

        // POST: LiveClass/Create
        [HttpPost]
        public ActionResult Add(LiveClasses liveClasses)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.LiveClasses.Add(liveClasses);
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                    return RedirectToAction("Index");
                }
            }
            catch
            {
            }
            ViewBag.SubjectId = new SelectList(db.Subject, "SubjectId", "Name");
            ViewBag.TeacherId = new SelectList(db.Teachers, "TeacherId", "FirstName");
            return View(liveClasses);
        }

        // GET: LiveClass/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LiveClasses liveClasses = db.LiveClasses.Find(id);
            if (liveClasses == null)
            {
                return HttpNotFound();
            }
            ViewBag.SubjectId = new SelectList(db.Subject, "SubjectId", "Name");
            ViewBag.TeacherId = new SelectList(db.Teachers, "TeacherId", "FirstName"); 
            return View(liveClasses);
        }

        // POST: LiveClass/Edit/5
        [HttpPost]
        public ActionResult Edit(LiveClasses liveClasses)
        {
            try
            {
                // TODO: Add update logic here
                if (ModelState.IsValid)
                {
                    db.Entry(liveClasses).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.SubjectId = new SelectList(db.Subject, "SubjectId", "Name");
                ViewBag.TeacherId = new SelectList(db.Teachers, "TeacherId", "FirstName");
                return View(liveClasses);
            }
            catch
            {
                return View();
            }
        }

        // GET: LiveClass/Delete/5
        [HttpGet]
            public ActionResult Delete(int id)
            {
                try
                {
                DateTime currentDate = DateTime.Today;
                var currentTime = DateTime.Now.TimeOfDay;
                LiveClasses liveClasses = db.LiveClasses.Find(id);
                if (liveClasses != null)
                {
                    if (liveClasses.Date >= currentDate)
                    {
                        if (liveClasses.Date == currentDate)
                        {
                            if (liveClasses.StartTime > currentTime)
                            {
                                db.LiveClasses.Remove(liveClasses);
                                db.SaveChanges();
                            }
                            else
                            {
                                TempData["failuremsg"] = "Completed Class Can Not be Deleted";
                                return RedirectToAction("Index");
                            }
                        }
                        else
                        {
                            db.LiveClasses.Remove(liveClasses);
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        TempData["failuremsg"] = "Completed Class Can Not be Deleted";
                    }
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }

        }
    }
}
